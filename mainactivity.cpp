#include "mainactivity.h"
#include <QDebug>

MainActivity::MainActivity(QWidget *centralWidget) : CustomActivity(centralWidget)
{
    mTxtGreeting = new QLabel(this);
    mBtnPlay = new QPushButton(this);
    mPresenter = new MainPresenter(this);
    mBtnPlay->setText("Play");
    mVLayout->addWidget(mTxtGreeting);
    mVLayout->addWidget(mBtnPlay);

    QObject::connect(mBtnPlay, SIGNAL(clicked()), this, SLOT(slotBtnPlay()));
}

void MainActivity::setGreetingText(QString s)
{
    mTxtGreeting->setText(s);
}

void MainActivity::slotBtnPlay()
{
    QObject * emetteur = sender();
    // emetteur contient le QPushButton btn si on clique sur ce bouton

    qDebug() << "hello";
    mPresenter->playPushed();
}

void MainActivity::startGameActivity(void)
{
    qDebug() << "MainActivity::startGameActivity()";
}

