#ifndef CUSTOMACTIVITY_H
#define CUSTOMACTIVITY_H

#include <QObject>
#include <QWidget>
#include <QVBoxLayout>

class CustomActivity : public QWidget
{
    Q_OBJECT
public:
    //explicit CustomActivity(QWidget *parent = nullptr);
    CustomActivity(QWidget *centralWidget = nullptr);
    QVBoxLayout* mVLayout;

private:

signals:

};

#endif // CUSTOMACTIVITY_H
