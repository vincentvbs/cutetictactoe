#include "mainpresenter.h"

MainPresenter::MainPresenter(MainView* mainView)
{
    mMainView = mainView;
    mMainView->setGreetingText("Welcome to my cute tictactoe!");
}

void MainPresenter::playPushed()
{
    mMainView->startGameActivity();
}
