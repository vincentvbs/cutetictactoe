#include "mainwindow.h"
#include <QDebug>
#include <QStyleFactory>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    setWindowTitle("cute tictactoe");
    qDebug() << "available styles:";
    qDebug() << QStyleFactory::keys();

    mCentralWidget = new QWidget(this);
    mMainActivity = new MainActivity(mCentralWidget);
    //mGameActivity = new GameActivity(mCentralWidget);

    setCentralWidget(mCentralWidget);
}

MainWindow::~MainWindow()
{
}

