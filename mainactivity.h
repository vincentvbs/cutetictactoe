#ifndef MAINACTIVITY_H
#define MAINACTIVITY_H

#include "customactivity.h"
#include "mainview.h"
#include "mainpresenter.h"
#include <QLabel>
#include <QPushButton>

class MainActivity : public CustomActivity,
                     public MainView
{
    Q_OBJECT
public:
    MainActivity(QWidget *centralWidget);


public slots:
    void slotBtnPlay();


private:
    QLabel* mTxtGreeting;
    QPushButton* mBtnPlay;
    MainPresenter* mPresenter;
    void startGameActivity(void);
    void setGreetingText(QString s);
};

#endif // MAINACTIVITY_H
