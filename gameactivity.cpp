#include "gameactivity.h"


GameActivity::GameActivity(QWidget *centralWidget) : CustomActivity(centralWidget)
{
    mTxtInfo = new QLabel(this);
    mVLayout->addWidget(mTxtInfo);
    mTxtInfo->setText("hello");

    mGridLayout = new QVBoxLayout;
    mVLayout->addLayout(mGridLayout);
    for(int i=0; i<9; i++)
    {
        QPushButton* btn = new QPushButton(this);
        mButtons.append(btn);
        mGridLayout->addWidget(btn);
        btn->setText("push");
    }


}



/*
mapper = new QSignalMapper();

// Bouton 1
connect(bouton1, SIGNAL(clicked()), mapper, SLOT(map()));
mapper->setMapping(bouton1, "http://url1");

// Bouton 2
connect(bouton2, SIGNAL(clicked()), mapper, SLOT(map()));
mapper->setMapping(bouton2, "http://url2");

// Autres boutons ?

Enfin, il suffit de connecter le signal mapped() de QSignalMapper à notre slot final. Ainsi quand un bouton émettra son signal, le slot openUrl() sera utilisé avec le paramètre correspondant au bouton.

Sélectionnez

connect(mapper, SIGNAL(mapped(const QString &)), this, SLOT(openUrl(const QString &)));
*/
