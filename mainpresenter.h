#ifndef MAINPRESENTER_H
#define MAINPRESENTER_H
#include "mainview.h"

class MainPresenter
{
public:
    MainPresenter(MainView* mainView);
    void playPushed();

private:
    MainView* mMainView;
};

#endif // MAINPRESENTER_H
