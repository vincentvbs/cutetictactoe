#ifndef MAINVIEW_H
#define MAINVIEW_H

#include <QString>

class MainView
{
public:
    //virtual MainView();

    virtual void setGreetingText(QString s) = 0;

    virtual void startGameActivity() = 0;

    /*
    void showInfo(QString s);

    void placeCircle(int buttonIndex);

    void placeCross(int buttonIndex);

    void clearSquare(int buttonIndex);
    */

};

#endif // MAINVIEW_H
