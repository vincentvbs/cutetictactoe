#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "mainactivity.h"
#include "gameactivity.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    QWidget* mCentralWidget;
    MainActivity* mMainActivity;
    GameActivity* mGameActivity;

};
#endif // MAINWINDOW_H
