#ifndef GAMEACTIVITY_H
#define GAMEACTIVITY_H

#include <QObject>
#include "customactivity.h"
#include <QLabel>
#include <QPushButton>

class GameActivity : public CustomActivity
{
    Q_OBJECT
public:
    GameActivity(QWidget *centralWidget);
private:
    QLabel* mTxtInfo;
    QVBoxLayout* mGridLayout;
    QList<QPushButton*> mButtons;
};

#endif // GAMEACTIVITY_H
